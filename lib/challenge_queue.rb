class ChallengeQueue

  # @param structure [String] A single or multi-line string of jobs
  # @return [ChallengeQueue] An instance of the challenge queue processor
  def initialize(structure = "")
    @structure = structure
    @queue  = []
  end

  # @return queue [Array] A list of the jobs in order of execution
  def process
    @structure.each_line do |line|
      @job, @dependency = extract(line)

      if job_without_dependency?
        add_job_to_queue
      elsif depends_on_itself?
        raise StandardError, "Job and dependency are the same."
      elsif both_already_queued?
        raise StandardError, "Job cannot have circular dependencies."
      else
        add_dependency_to_queue
        add_job_to_queue
      end

    end
    @queue.compact
  end

  private

  # @param line [String] A single line from the structure
  # @return [Array] An array containing just the job, or a job and dependency
  def extract(line)
    line.split(/\s/).reject { |l| l.empty? || l.match("=>") }
  end

  # @return [Boolean] Does this job have a dependency?
  def job_without_dependency?
    @dependency.nil?
  end

  # @return [Boolean] Are the job and dependency the same?
  def depends_on_itself?
    @job == @dependency
  end

  # @return [Boolean] Does the queue already contain the job?
  def job_already_queued?
    @queue.include?(@job)
  end

  # @return [Boolean] Does the queue already contain the dependency?
  def dependency_already_queued?
    @queue.include?(@dependency)
  end

  # @return [Boolean] Does the queue already contain both job and dependency?
  def both_already_queued?
    job_already_queued? && dependency_already_queued?
  end

  # @return [Array] The queue with the job added, unless it already exists
  def add_job_to_queue
    @queue << @job unless job_already_queued?
  end

  # @return [Array] The queue with the dependency added, unless it already exists
  def add_dependency_to_queue
    @queue.unshift(@dependency) unless dependency_already_queued?
  end

end