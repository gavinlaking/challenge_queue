require "spec_helper"

describe ChallengeQueue do
  let(:structure)       { "" }
  let(:challenge_queue) { ChallengeQueue.new(structure) }
  let(:queue)           { [] }

  context ".initialize" do
    subject { ChallengeQueue.new(structure) }

    it "assigns the @structure instance variable" do
      subject.instance_variable_get("@structure").should == structure
    end
    it "assigns the @queue to an empty array" do
      subject.instance_variable_get("@queue").should == queue
    end
  end

  context "#process" do
    subject { ChallengeQueue.new(structure).process }

    context "when the job structure is empty" do
      let(:structure) { "" }
      it "the result is an empty sequence (Q1)" do
        subject.should eq []
      end
    end

    context "with a single line job structure with no dependencies" do
      let(:structure) { "a =>" }
      it "returns a single job (Q2)" do
        subject.should eq ["a"]
      end
    end

    context "with a multi-line job structure" do
      context "with no dependencies" do
        let(:structure) { "a =>\nb =>\nc =>" }
        it "returns multiple jobs (Q3)" do
          subject.should eq ["a", "b", "c"]
        end
      end

      context "with single dependency" do
        let(:structure) { "a =>\nb => c\nc =>" }
        it "returns multiple jobs, honouring the dependency (Q4)" do
          subject.should eq ["c", "a", "b"]
        end
      end

      context "with multiple dependencies" do
        let(:structure) { "a =>\nb => c\nc => f\nd => a\ne => b\nf =>" }
        it "returns multiple jobs, honouring the dependencies (Q5)" do
          subject.should =~ ["f", "c", "b", "e", "a", "d"]
        end
      end

      context "with the job and dependency the same" do
        let(:structure) { "a =>\nb =>\nc => c" }
        it "raises an error stating that jobs cannot depend on themselves (Q6)" do
          expect { subject }.to raise_exception StandardError, /are the same/
        end
      end

      context "with circular dependencies" do
        let(:structure) { "a =>\nb => c\nc => f\nd => a\ne =>\nf => b" }
        it "raises an error stating that jobs can't have circular dependencies (Q7)" do
          expect { subject }.to raise_exception StandardError, /circular dependencies/
        end
      end
    end

  end
end
